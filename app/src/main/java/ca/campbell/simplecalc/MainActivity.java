package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    public void addNums(View v) {
        double num1 = getDoubleFromEt(etNumber1);
        double num2 = getDoubleFromEt(etNumber2);
        displayResult(num1 + num2);

    }  //addNums()

    public void subNums(View v) {
        double num1 = getDoubleFromEt(etNumber1);
        double num2 = getDoubleFromEt(etNumber2);
        displayResult(num1 - num2);
    }

    public void multiplyNums(View v) {
        double num1 = getDoubleFromEt(etNumber1);
        double num2 = getDoubleFromEt(etNumber2);
        displayResult(num1 * num2);
    }

    public void divideNum(View v) {
        double num1 = getDoubleFromEt(etNumber1);
        double num2 = getDoubleFromEt(etNumber2);
        if (num2 != 0) {
            displayResult(num1 / num2);
        } else {
            result.setText(R.string.DivideByZero);
        }
    }

    public void clear(View v) {
        result.setText("");
        etNumber2.setText("");
        etNumber1.setText("");
    }

    public void displayResult(double doubleResult) {
        result.setText(String.format("%f", doubleResult));
    }

    public double getDoubleFromEt(EditText et) {
        String input = et.getText().toString();

        if (input.isEmpty()) {
            return 0;
        }

        return Double.parseDouble(input);
    }
}